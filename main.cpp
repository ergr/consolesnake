#include <iostream>
#include <ncurses.h>
#include <unistd.h>

using namespace std;

bool quitGame = false;
bool gameOver = false;
bool restartGame = false;

const int width = 20;
const int heigth = 20;
int x = 0;
int y = 0;
int prevX[50];
int prevY[50];
int fruitX = 0;
int fruitY = 0;
int score = 0;
enum eDirection {STOP = 0, LEFT, RIGHT, UP, DOWN};
eDirection dir;

void cleanupNcurses()
{
    endwin();
}

void initNcurses()
{
    atexit(cleanupNcurses);

    initscr();
	noecho();
    nodelay(stdscr, TRUE); // The getch will be a non-blocking function, but returns ERR if no input is provided.
    keypad(stdscr, TRUE);  // Enable the additionals keys above ASCII (>255) - e.g. arrow keys.
    curs_set(0);           // Set cursor to be invisible.
}

void respawnFruit()
{
    fruitX = 1 + (rand() % (width -2)); // Due to the border, the effective-arena-width is only from 1 to (width-1) 
    fruitY = 1 + (rand() % (heigth-2)); // Due to the border, the effective-arena-height is only from 1 to (height-1)
}

void initGame()
{
    gameOver = false;
    quitGame = false;
    restartGame = false;
    dir = STOP;
    x = width  / 2;
    y = heigth / 2;
    score = 0;

    respawnFruit();
}

void prepareSnakeTailForNextStep()
{
    if(score > 1)
    {
        for (int i = 0; i < score; i++)
        {
            prevX[score - (1 + i)] = prevX[score - (2 + i)];
            prevY[score - (1 + i)] = prevY[score - (2 + i)];
        }
    }

    prevX[0] = x;    
    prevY[0] = y;
}

void moveSnakeOneStep()
{
    switch (dir)
    {
    case LEFT:  x--; break;
    case RIGHT: x++; break;
    case UP:    y--; break;
    case DOWN:  y++; break;
    default:
        break;
    }
}

void passSnakeThroughWalls()
{
    if(x < 1)
    {
        x = width - 2;
    }
    else if(x >= width-1)
    {
        x = 1;
    }

    if(y < 1)
    {
        y = heigth-1;
    }
    else if (y >= heigth)
    {
        y = 1;
    }
}

void moveSnake()
{
    prepareSnakeTailForNextStep();

    moveSnakeOneStep();

    passSnakeThroughWalls();
}

bool foundFruit()
{
    bool found = false;

    if(x == fruitX && y == fruitY)
    {
        found = true;
    }

    return found;
}

bool hitTail()
{
    bool hit = false;

    for(int i = 0; i < score; i++)
    {
        if(prevX[i] == x && prevY[i] == y)
        {
            hit = true;
            break;
        }
    }

    return hit;
}

void processGame()
{
    moveSnake();

    if(foundFruit())
    {
        score++;
        respawnFruit();
    }

    if(hitTail())
    {
        gameOver = true;
    }
}

void changeDirectionIfAllowedTo(eDirection requestedDir)
{
    switch (requestedDir)
    {
    case LEFT:
        if (score == 0 || dir != RIGHT)
        {
            dir = LEFT;
        }
        break;

    case RIGHT:
        if (score == 0 || dir != LEFT)
        {
            dir = RIGHT;
        }
        break;

    case UP:
        if (score == 0 || dir != DOWN)
        {
            dir = UP;
        }
        break;

    case DOWN:
        if (score == 0 || dir != UP)
        {
            dir = DOWN;
        }
        break;

    default:
        break;
    }
}

void evaluateInputs()
{
    int key = getch();
    if (ERR != key)
    {
        switch (key)
        {
        case 'a':  
        case KEY_LEFT:  changeDirectionIfAllowedTo(LEFT);  break;

        case 'd':  
        case KEY_RIGHT: changeDirectionIfAllowedTo(RIGHT); break;

        case 'w':  
        case KEY_UP:    changeDirectionIfAllowedTo(UP);    break;

        case 's':  
        case KEY_DOWN:  changeDirectionIfAllowedTo(DOWN);  break;

        case 'o':       
            gameOver = true;
            restartGame = false;
            break;
        
        case 'q':       
            quitGame = true;
            restartGame = false;
            break;
        
        default:
            restartGame = true;
            break;
        }
    }
}

void renderGame()
{
    clear();

    // Draw upper border of arean.
    for(int i=0; i<width; i++)
    {
        mvprintw(0, i, "#");
    }

    // Draw both sides of arena.
    for(int j=0; j<heigth; j++)
    {
        for(int i=0; i<width; i++)
        {
            if(0 == i)
            {
                mvprintw(j, i, "#");
            }

            if(width-1 == i)
            {
                mvprintw(j, i, "#");
            }
        }
    }

    // Draw bottom border of arena.
    for (int i = 0; i < width; i++)
    {
        mvprintw(heigth, i, "#");
    }

    // Draw Fruit
    mvprintw(fruitY, fruitX, "*");

    // Draw the snake's tail.
    for (int k = 0; k < score; k++)
    {
        mvprintw(prevY[k], prevX[k], "o");
    }

    // Draw the snake's head.
    mvprintw(y, x, "O");

    // Draw SCORE
    mvprintw(0, 5, " SCORE %d ", score);

    if (gameOver)
    {
        mvprintw(y, x, "Autsch !!");
    }

    refresh();
}

void sleepMilliseconds(int ms)
{
    timespec timeSleep  = {ms/1000, (ms%1000)*1000000}; // {tv_sec, tv_nsec}
    timespec timeRemain = {0, 0};

    nanosleep(&timeSleep, &timeRemain);
}

void restartOrQuitGame()
{
    static const char gameoverText[] = "GAME OVER";
    static       int  gameoverTextY  = heigth/4;
    static const int  gameoverTextX  = width/2-5;

    static const char restartText[]      = "Restart Game [SPACE]";
    static const char restartTextBlank[] = "                    ";
    static       int  restartTextY = gameoverTextY + 1;
    static const int  restartTextX = width / 2 - 10;

    static const char quitText[]      = "Quit Game [q]";
    static const char quitTextBlank[] = "             ";
    static       int  quitTextY = gameoverTextY + 2;
    static const int  quitTextX = width / 2 - 7;

    // Print message not over snake's head.
    if(y < heigth/2)
    {
        gameoverTextY = heigth*3/4;
        restartTextY  = gameoverTextY + 1;
        quitTextY     = gameoverTextY + 2;
    }

    restartGame = false;
    quitGame    = false;

    mvprintw(gameoverTextY, gameoverTextX, " GAME OVER ");

    do
    {
        mvprintw(restartTextY, restartTextX, "%s", restartText);
        mvprintw(quitTextY,    quitTextX,    "%s", quitText);
        refresh();
        sleepMilliseconds(600);
        mvprintw(restartTextY, restartTextX, "%s", restartTextBlank);
        mvprintw(quitTextY,    quitTextX,    "%s", quitTextBlank);
        refresh();
        sleepMilliseconds(200);
        evaluateInputs();
    } while(!restartGame && !quitGame);
}

void runGame()
{
    while (!quitGame)
    {
        evaluateInputs();
        processGame();
        renderGame();
        sleepMilliseconds(80);

        if(gameOver)
        {
            restartOrQuitGame();

            if(restartGame)
            {
                initGame();
            }
        }
    }
}

int main(void)
{
    cout << "start Game." << endl;

    initNcurses();
    initGame();
    runGame();

    return 0;
}
