# ConsoleSnake

A simple snake clone that runs on the console. It is written in C++ and uses the ncurses library. It was developed with the MS-WSL Ubuntu 18.04 and VSCode.

## Application

To run the application type:

    ./consolesnake

### Usage

Navigate the snakes head 'O' with the cursor keys `UP`, `DOWN`, `LEFT`, `RIGHT` or the typical used gamer letters `w`, `a`, `s` and `d`. 

`o` cancels a running game.
`q` quits the game back to the console.

## Target System

Linux

## Build

### Executable

To build the application type:

    make

### Documentation

Currently only this README.md exists.

### Unittests

(tbd)

## Prerequisites

### Depending libraries

    -lncurses

Visit the makefile. Check the LDFLAGS for the complete list of used libraries.

To install the libraries on Linux Ubuntu 18.04 

    sudo apt install libncurses5-dev

### Tool-chain

To build and debug the executable 

1. g++
2. make
3. gdb

To install the tool-chain on Linux Ubuntu 18.04

    sudo apt install gcc make gdb

## IDE

The code was developed with the VSCode IDE.

## Architecture and design

The design goal is have the most simple approach to a console game.

The commonly wide used library "ncurses" is used to simplify keyboard inputs and have direct printing to a coordinate system. There is some space left for improvements for some better techniques using the ncurses library, e.g. Windows and draw the box with one call.

It is only one 'big' file. The states and modes are handled by module global variables.
This leaves some space left for future improvements.
