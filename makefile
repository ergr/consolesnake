DEBUG=-g
CC=g++
CXXFLAGS=$(DEBUG)
LDFLAGS=-lncurses

consolesnake : main.o
	$(CC) $^ -o $@ $(LDFLAGS)

%.o : %.cpp
	$(CC) -c $(CXXFLAGS) $(CPPFLAGS) $< -o $@

clean:
	rm -rf consolesnake main.o
